#!/bin/bash

if [ $# -ne 2 ]; then
	echo "Wrong number of parameters"
	exit;
fi;

FILENAME="$1"
LINENUMBER=$2
ELINENUMBER=${LINENUMBER}
bcount=0

sed -n ''$LINENUMBER',$p' <"$FILENAME" | \
while IFS= read -r -n1 -d '' char; do
	if [ "$char" == '{' ]
	 then
		(( bcount = bcount + 1 ));
	elif [ "$char" == '}' ]
	 then
		(( bcount = bcount - 1 ))
		if [ "$bcount" -le "0" ]
		 then
			sed ''$LINENUMBER','$ELINENUMBER's/^/\/\//' -i $FILENAME
			exit;
		fi
	elif [ "$char" == $'\n' ]
	 then
		(( ELINENUMBER = ELINENUMBER + 1 ));
	fi
done
