#!/bin/bash

# Auto-delete compressed folders

DIR="$1"

rm "${DIR}"/*.zip
rm "${DIR}"/*.tar.gz
