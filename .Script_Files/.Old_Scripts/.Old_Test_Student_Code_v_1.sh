#!/bin/bash

GRE='\033[0;32m'
LGRE='\033[1;32m'
NC='\033[0m'
LABDIR=""
SCDIR=""
INPUTCHECK=1

echo -e "${LGRE}Welcome ${GRE}$USER${NC}"

while [[ $INPUTCHECK = 1 ]]; do
	read -p "Enter Lab Number: " n
	if [[ $n = [1-9] ]]
	then
		LABDIR="${PWD}""/.Labs/$n"
		SCDIR="${LABDIR}""/Student_Code"
		INPUTCHECK=0
	else
		echo -e "\033[0;33m]\"$n\" is not a valid lab number${NC}"
	fi
done

echo
echo -e "${LGRE}Decompressing student code...${NC}"

echo -e "${GRE}	Looking for .zip archives...${NC}"
find . -type f -name '*.zip' -print0 | while read -r -d '' filename; do
unzip "${filename}" '*.h' '*.cpp' -d "${SCDIR}"; done

echo -e "${GRE}	Looking for .tar.gz archives...${NC}"
tar -v -C "${SCDIR}" -zxvf *.tar.gz *.cpp *.h --transform='s/.\///'

if [[ "$(ls -A "${SCDIR}")" ]]; then
	echo -e "${LGRE}Student code decompressed${NC}"
	echo
	echo -e "${LGRE}Beginning pass-off attempt...${NC}"
	
	echo -e "${GRE}	Reorganizing code directory structure...${NC}"
	find . -type f \( -name '*.h' -or -name '*.cpp' \) -print \
		-exec mv -f {} "${SCDIR}" &>/dev/null \;

	echo -e "${GRE}	Running test driver for Lab $n...${NC}"
	cd "${LABDIR}"
	echo
	bash Run_Test_Driver.sh
	echo

	echo -e "${GRE}	Displaying student code...${NC}"
	echo -e "${GRE}	Playing Minecraft...${NC}"
	gedit --new-window "${SCDIR}"/*.*

	echo -e "${GRE}	Cleaning up...${NC}"
	rm "${SCDIR}" -r -v
	mkdir "${SCDIR}"

	echo -e "${LGRE}Done!${NC}"
else
	echo -e "${LGRE}No student code found${NC}"
	read -p "Press any key to exit..."
fi;
