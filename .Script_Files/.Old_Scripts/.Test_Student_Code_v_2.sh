#!/bin/bash

# Test_Student_Code script, version 2.0

GRE='\033[0;32m'
LGRE='\033[1;32m'
NC='\033[0m'
DLABDIR="${PWD}/.Labs"
DSCDIR="${PWD}/.Student_Code"

echo -e "${LGRE}Welcome ${GRE}$USER${NC}"
echo
echo -e "${LGRE}Decompressing student code...${NC}"

echo -e "${GRE}	Looking for .zip archives...${NC}"
find . -type f -name '*.zip' -print0 | while read -r -d '' filename; do
unzip "${filename}" '*.h' '*.cpp' -d "${DSCDIR}" &>/dev/null ; done

echo -e "${GRE}	Looking for .tar.gz archives...${NC}"
tar -v -C "${DSCDIR}" -zxvf *.tar.gz *.cpp *.h &>/dev/null

if [[ "$(ls -A "${DSCDIR}")" ]]; then
	echo -e "${LGRE}Student code decompressed${NC}"
	echo
	echo -e "${LGRE}Beginning pass-off attempt...${NC}"
	
	echo -e "${GRE}	Reorganizing code directory structure...${NC}"
	find . -type f \( -name '*.h' -or -name '*.cpp' \) | \
		xargs -I '{}' mv -f {} "${DSCDIR}" &>/dev/null

	echo -e "${GRE}	Divining lab number...${NC}"
	LABNUMBER=$(grep -oP "(?<=\").*\.h" "${DSCDIR}/Factory.h" | \
		grep -n -f /dev/stdin "${DLABDIR}/.LabKeys.txt" | cut -f1 -d:)
	LABDIR="${DLABDIR}/${LABNUMBER}"
	SCDIR="${LABDIR}/Student_Code"

	echo -e "${GRE}	Setting up lab ${LABNUMBER}...${NC}"
	mv -f "${DSCDIR}"/* "${SCDIR}"

	echo -e "${GRE}	Running test driver for Lab ${LABNUMBER}...${NC}"
	cd "${LABDIR}"
	echo
	bash Run_Test_Driver.sh
	echo

	echo -e "${GRE}	Displaying student code...${NC}"
	echo -e "${GRE}	Playing Minecraft...${NC}"
	bluefish --new-window "${SCDIR}"/*.* &>/dev/null

	echo -e "${GRE}	Cleaning up...${NC}"
	rm "${SCDIR}" -r
	mkdir "${SCDIR}"

	echo -e "${LGRE}Done!${NC}"
else
	echo -e "${LGRE}No student code found${NC}"
	read -p "Press any key to exit..."
fi;
