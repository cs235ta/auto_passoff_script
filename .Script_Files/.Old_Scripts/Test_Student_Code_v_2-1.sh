#!/bin/bash

# Test_Student_Code script, version 2.1

source "${PWD}/.Script_Files/Settings.cfg"

echo -e "${SECCOLOR}Welcome ${PRICOLOR}$USER${NOCOLOR}"
echo
echo -e "${SECCOLOR}Decompressing student code...${NOCOLOR}"

echo -e "${PRICOLOR}	Looking for .zip archives...${NOCOLOR}"
find . -type f -name '*.zip' -print0 | while read -r -d '' filename; do
unzip "${filename}" '*.h' '*.cpp' -d "${PRESCDIR}" &>/dev/null ; done

echo -e "${PRICOLOR}	Looking for .tar.gz archives...${NOCOLOR}"
tar -v -C "${PRESCDIR}" -zxvf *.tar.gz *.cpp *.h &>/dev/null

if [[ "$(ls -A "${PRESCDIR}")" ]]; then
	echo -e "${SECCOLOR}Student code decompressed${NOCOLOR}"
	echo
	echo -e "${SECCOLOR}Beginning pass-off attempt...${NOCOLOR}"
	
	echo -e "${PRICOLOR}	Reorganizing code directory structure...${NOCOLOR}"
	find . -type f \( -name '*.h' -or -name '*.cpp' \) | \
		xargs -I '{}' mv -f {} "${PRESCDIR}" &>/dev/null

	echo -e "${PRICOLOR}	Checking for and neutralizing main functions...${NOCOLOR}"
	grep -Hn '\s*int\s*main\s*\(.*\)' "${PRESCDIR}"/*.* | \
		while read -r line; do
			FIRST=$(echo ${line} | cut -d: -f1)
			SECOND=$(echo ${line} | cut -d: -f2)
			bash "${SCRIPTDIR}"/Comment_Main.sh "${FIRST}" "${SECOND}";
		done

	echo -e "${PRICOLOR}	Divining lab number...${NOCOLOR}"
	LABNUMBER=$(grep -oP "(?<=\").*\.h" "${PRESCDIR}/Factory.h" | \
		grep -n -f /dev/stdin "${PREPLABDIR}/.LabKeys.txt" | cut -f1 -d:)
	LABDIR="${PREPLABDIR}/${LABNUMBER}"
	SCDIR="${LABDIR}/Student_Code"

	echo -e "${PRICOLOR}	Assuring clean code folder...${NOCOLOR}"
	if [[ "$(ls -A "${SCDIR}")" ]]
	 then
		rm "${SCDIR}" -r
		mkdir "${SCDIR}"
	fi

	echo -e "${PRICOLOR}	Setting up lab ${LABNUMBER}...${NOCOLOR}"
	mv -f "${PRESCDIR}"/* "${SCDIR}"
	rm "${PRESCDIR}" -r
	mkdir "${PRESCDIR}"

	echo -e "${PRICOLOR}	Running test driver for Lab ${LABNUMBER}...${NOCOLOR}"
	cd "${LABDIR}"
	echo
	timeout 5s ./Run_Test_Driver.sh
	echo

	echo -e "${PRICOLOR}	Displaying student code...${NOCOLOR}"
	echo -e "${PRICOLOR}	Playing Minecraft...${NOCOLOR}"
	bluefish --new-window "${SCDIR}"/*.* &>/dev/null

	echo -e "${PRICOLOR}	Cleaning up...${NOCOLOR}"
	rm "${SCDIR}" -r
	mkdir "${SCDIR}"

	shopt -s nocasematch
	if [[ "${DELSET}" == "auto" ]]
	 then
		bash "${AUTODSCRIPT}" "${BASEDIR}"
	elif [[	"${DELSET}" == "ask" ]]
	 then
		bash "${ASKDSCRIPT}" "${BASEDIR}"
	fi

	echo -e "${SECCOLOR}Done!${NOCOLOR}"
else
	echo -e "${SECCOLOR}No student code found${NOCOLOR}"
	read -p "Press any key to exit..."
fi;
