#!/bin/bash

# Ask delete compressed folders

LGRE='\033[1;32m'
NC='\033[0m'
DIR="$1"

echo -e "${LGRE}Delete all compressed files? (y/n)${NC}"
read -p "" yn

if [ "$yn" == 'y' ] || [ "$yn" == 'Y' ]
 then
	rm "${DIR}"/*.zip
	rm "${DIR}"/*.tar.gz
fi
